﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace DevcoinExchange
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }

        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");



            //routes.MapRoute(
            //        name: "Index",
            //        url: "Index",
            //        defaults: new { controller = "Home", action = "Index" });

            routes.MapRoute(
                     name: "Trade",
                     url: "Trade",
                     defaults: new { controller = "Trade", action = "Trade" });
           
            routes.MapRoute(
                       name: "Compare-Exchanges",
                       url: "Compare-Exchanges",
                       defaults: new { controller = "Home", action = "CompareExchanges" });

            routes.MapRoute(
                    name: "Login",
                    url: "Login",
                    defaults: new { controller = "Home", action = "Login" });

            routes.MapRoute(
                    name: "Report-a-problem",
                    url: "Report-a-problem",
                    defaults: new { controller = "Home", action = "ReportAProblem" });
            
            routes.MapRoute(
                         "Default", // Route name
                         "{controller}/{action}/{id}", // URL with parameters
                         new { controller = "Home", action = "Index", id = UrlParameter.Optional } // Parameter defaults
                     );


            //<li><a href="http://@Request.Url.Authority/Trade">Trade</a></li>
            //    <li><a href="http://@Request.Url.Authority/Compare-Exchanges">Compare Exchanges</a></li>
            //    <li><a href="http://@Request.Url.Authority/Login">Login</a></li>
            //    <li><a href="http://@Request.Url.Authority/Report-a-problem">Report a problem</a></li>

        }

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            // Use LocalDB for Entity Framework by default
            Database.DefaultConnectionFactory = new SqlConnectionFactory(@"Data Source=(localdb)\v11.0; Integrated Security=True; MultipleActiveResultSets=True");

            RegisterGlobalFilters(GlobalFilters.Filters);
            RegisterRoutes(RouteTable.Routes);
        }
    }
}