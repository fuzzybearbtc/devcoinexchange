﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DevcoinExchange.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/

        public ActionResult Index()
        {
            return View("Index");
        }

        

        public ActionResult Login()
        {
            return View("Login");
        }

        public ActionResult CompareExchanges()
        {
            return View("CompareExchanges");
        }

        public ActionResult ReportAProblem()
        {
            return View("ReportAProblem");
        }

    }
}
